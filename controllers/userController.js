// Export the schema models
const User = require("../models/User");
const Product = require("../models/Product");

// Add Product and Order later

// Add bcrypt and auth later 
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register / create a user
module.exports.createUser = (enterDetails) =>{
	let newUser = new User ({
		firstName: enterDetails.firstName,
		lastName: enterDetails.lastName,
		address: enterDetails.address,
		email: enterDetails.email,
		password: bcrypt.hashSync(enterDetails.password, 10), 
		paymentOption: enterDetails.paymentOption
	})
	return newUser.save()
	.then((result, error)=>{
		if(error){
			return false;
		}else{
			return result
		}
	})
}

// Get all users
module.exports.getAllUsers = () =>{
	return User.find({})
	.then(result =>{
		return result;
	})
}

// Set User as Admin
module.exports.makeUserAdmin = (userParams)=>{

	let updatedStatus = {
		isAdmin: true
	}
	console.log(updatedStatus)
	return User.findByIdAndUpdate(userParams.userId, updatedStatus)
	.then((updated, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}

// Create a User Authentication upon Login
module.exports.customerLogin = (enterLoginDetails)=>{
	return User.findOne({email: enterLoginDetails.email})
	.then(result =>{
		if(result === null){
			return false
		}else{
			const isPasswordSame = bcrypt.compareSync(enterLoginDetails.password, result.password)
				if(isPasswordSame){
					console.log(isPasswordSame)
					return { customerAccessToken: auth.createCustomerAccessToken(result.toObject())} // gagawan ng token ang laman ng data sa auth.js
				}else{
					return false
				}
		}
	})
}

// User purchasing a product
module.exports.buyProduct = async(data)=>{

	let isUserOrdered = await User.findById(data.userId)
	.then(user=>{
		user.purchases.push({
						  productId: data.productId,
						  price: data.price,
						  quantity: data.quantity, 
						  totalAmount: data.totalAmount
						})
		//let totalAmount = data.quantity * data.price
		return user.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	let isProductOrdered = await Product.findById(data.productId)
	.then(product=>{
		product.orders.push({userId: data.userId})
		return product.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	if(isUserOrdered && isProductOrdered){
		return true
	}else{
		return false
	}
}
