// Export the product schema model
const Product = require("../models/Product");

// Add User and Order later

// Create a product 
module.exports.createProduct = (info)=>{				
	
	if(info.isAdmin){
		let newProduct = new Product({
			name: info.product.name,
			description: info.product.description,
			price: info.product.price
		})
		return newProduct.save()
		.then((result, error)=>{
			if(error){
				return false
			}else{
				return result
			}
		})
	}else{
		return false
	}	
}

// Retrieve All Products
module.exports.getAllProducts = () =>{
	return Product.find({})
	.then(result=>{
		return result
	})
}

// Retrieve a Single Product
module.exports.specificProduct = (idParam)=>{
	return Product.findById(idParam.productId)
	.then(result=>{
		return result
	})
}

// Update Product Information
module.exports.updateProduct = (productParams, reqBody) =>{
	let updateProductInfo = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(productParams.productId, updateProductInfo)
	.then((product, err)=>{
		if(err){
			return false
		}else{
			return "Updated successfully!"
		}
	})
}

// Archiving a Product using an Admin User
module.exports.archiveProduct = (productParams) =>{
	let archived = {
		isActive: false
	}
	return Product.findByIdAndUpdate(productParams.productId, archived)
	.then((result, err)=>{
		if(err){
			return false
		}else{
			return "Archived successfully!"
		}
	})
}