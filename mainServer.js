// Require express
const express = require("express");
const app = express();
// Require Mongoose
const mongoose = require("mongoose");
// Require cors for endpoint restriction
const cors = require("cors");

// Require all routes.js file
	// Require route for user
const userRoute = require("./routes/userRoute");
	// Require route for product
const productRoute = require("./routes/productRoute");
	// Require route for product
const orderRoute = require("./routes/orderRoute");

// Connect to MongoDB Database (Cloud) Connect > Connect your Application > Coppy connection string and paste it here
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z9use.mongodb.net/All_Resto_E_Commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// No endpoint restrictions
app.use(cors());

// Allow the server(app) to read json data
app.use(express.json())
//Allow the server(app) to read all data types
app.use(express.urlencoded({extended:true}));

// Create an error handling if we successfully connected to our database or not
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Failed to connect to the cloud database"));
db.once("open", () => console.log("Connected successfully to cloud database"))

// Create a (base URL, connected route) that will apply the base url
	// Base url for user
app.use("/users", userRoute);
	// Base url for product
app.use("/products", productRoute);
	// Base url for product
app.use("/orders", orderRoute);

app.listen(process.env.PORT || 4000, ()=>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})