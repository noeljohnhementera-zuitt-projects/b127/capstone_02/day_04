// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the user controller
const orderController = require("../controllers/orderController");

// Export auth.js for user verification
const auth = require("../auth");

// Create Order (Non-admin)
router.post("/addOrder", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	if(data.userId){
		orderController.addOrder(data, req.body)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Get all customer orders (Admin only)
router.get("/all-admin", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	if(data.isAdmin){
		orderController.getAllOrder(data)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Get authenticated User's Order (Non-admin)
router.get("/all-non-admin/:id", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	if(data.userId){
		orderController.getVerifiedUserOrder(data, req.params.id)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Create authenticated User Id Order Transaction
router.post("/transaction/:id", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity,
		totalAmount: req.body.quantity * req.body.price
	}
	console.log(data)

	if(data.userId){
		orderController.seeOrderDetails(data, req.params.id)
			.then(details =>
				res.send(details))
	}else{
		res.send(false)
	}
	
})

// MISCELLANEOUS ROUTE

// Make Admin user as Admin
router.put("/:customerId/status-updated", (req, res)=>{
	orderController.makeUserAdmin(req.params)
	.then(result =>{
		res.send(result)
	})
})


/*----------------------------------------------------------*/
// NOT PART OF THE REQUIREMENT (Just Random Controllers hehe)

// Get Single Order (Non-admin)
router.get("/track-order/:id", (req, res)=>{
	orderController.trackOrder(req.params.id)
	.then(result =>
		res.send(result))
})

// Add Authenticated User's orders
router.post("/addedVerifiedUserOrder", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		product: req.body
	}
	console.log(data)

		if(data.isAdmin){
			orderController.getverifiedUserOrder(data)
			.then(result=>{
				res.send(result)
			})
		}else{
			res.send(false)
		}
	
})

// Get Authenticated User's orders
router.get("/getVerifiedUserOrder/:id", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

		if(data.isAdmin){
			orderController.addVerifiedUserOrder(data, req.params.id)
			.then(result=>
				res.send(result))
		}else{
			res.send(false)
		}
})

// Get All orders (Admin User)
router.get("/admin", (req, res)=>{
	orderController.getAdminUserOrder()
	.then(result=>
		res.send(result))
})

module.exports = router;