// Require express
const express = require("express");
// Create a router
const router = express.Router();

//Export auth.js
const auth = require("../auth")

// Export the user controller
const userController = require("../controllers/userController");
// Export auth for user access


// Create a User
router.post("/register", (req, res)=>{
	userController.createUser(req.body)
	.then(userCreated =>
		res.send(userCreated))
})

// Get all users
router.get("/all", (req, res)=>{
	userController.getAllUsers()
	.then(result =>{
		res.send(result);
	})
})

// Create an Admin User
router.put("/:userId/status-updated", (req, res)=>{
	userController.makeUserAdmin(req.params)
	.then(result =>{
		res.send(result)
	})
})

// Create a User Authentication upon Login
router.post("/login", (req, res)=>{
	userController.customerLogin(req.body)
	.then(result=>{
		res.send(result)
	})
})

// Authenticated User purchasing a product
router.post("/orderSuccessful", auth.verify, (req, res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity,
		totalAmount: req.body.quantity * req.body.price
	}
	console.log(data)

	if(data.userId){
		userController.buyProduct(data)
			.then(orders =>
				res.send(orders))
	}else{
		res.send(false)
	}
	
})

module.exports = router;